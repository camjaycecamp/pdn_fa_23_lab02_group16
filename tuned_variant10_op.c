/*
  This is the baseline implementation of a 1D Stencil operation.

  Parameters:

  m0 > 0: dimension of the original input and output vector(array) size
  k0 > 0: dimesnion of the original weights vector(array)

  float* input_sequential: pointer to original input data
  float* input_distributed: pointer to the input data that you have distributed across
  the system

  float* output_sequential:  pointer to original output data
  float* output_distributed: pointer to the output data that you have distributed across
  the system

  float* weights_sequential:  pointer to original weights data
  float* weights_distributed: pointer to the weights data that you have distributed across
  the system

  Functions: Modify these however you please.

  DISTRIBUTED_ALLOCATE_NAME(...): Allocate the distributed buffers.
  DISTRIBUTE_DATA_NAME(...): takes the sequential data and distributes it across the system.
  COMPUTE_NAME(...): Performs the stencil computation.
  COLLECT_DATA_NAME(...): Collect the distributed output and combine it back to the sequential
  one for testing.
  DISTRIBUTED_FREE_NAME(...): Free the distributed buffers that were allocated


  - richard.m.veras@ou.edu

*/

#include <immintrin.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef COMPUTE_NAME
#define COMPUTE_NAME baseline
#endif

#ifndef DISTRIBUTE_DATA_NAME
#define DISTRIBUTE_DATA_NAME baseline_distribute
#endif

#ifndef COLLECT_DATA_NAME
#define COLLECT_DATA_NAME baseline_collect
#endif

#ifndef DISTRIBUTED_ALLOCATE_NAME
#define DISTRIBUTED_ALLOCATE_NAME baseline_allocate
#endif

#ifndef DISTRIBUTED_FREE_NAME
#define DISTRIBUTED_FREE_NAME baseline_free
#endif



void COMPUTE_NAME(int m0, int k0, float *input_distributed, float *weights_distributed, float *output_distributed)
{
  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if (rid == root_rid)
  {
    // Parallelize the outer loop with OpenMP
#pragma omp parallel for
    // Loop unrolling (ILP) for vectorized computation
    for (int i0 = 0; i0 < m0; i0 += 4)
    {
      // Initialize four accumulators to zero
      __m256 sum[4] = {_mm256_setzero_ps(), _mm256_setzero_ps(), _mm256_setzero_ps(), _mm256_setzero_ps()};

      // Vectorized computation of the dot product
      for (int p0 = 0; p0 < k0 - 7; p0 += 8)
      {
        __m256 a[4], b[4];

        // Load four values from input and weights into vectors
        for (int j = 0; j < 4; ++j)
        {
          a[j] = _mm256_loadu_ps(&input_distributed[((p0 + i0 + j) % m0)]);
          b[j] = _mm256_loadu_ps(&weights_distributed[p0]);
        }

        // Compute the dot product for each of the four accumulators
        for (int j = 0; j < 4; ++j)
        {
          sum[j] = _mm256_add_ps(sum[j], _mm256_mul_ps(a[j], b[j]));
        }
      }

      // Reduce the accumulators and store the results in a local array
      float res[4] = {0.0f, 0.0f, 0.0f, 0.0f};
      for (int j = 0; j < 4; ++j)
      {
        sum[j] = _mm256_hadd_ps(sum[j], sum[j]);
        sum[j] = _mm256_hadd_ps(sum[j], sum[j]);
        res[j] = sum[j][0] + sum[j][1] + sum[j][2] + sum[j][3];
      }

      // Handle the remaining elements using a scalar loop
      for (int j = 0; j < 4; ++j)
      {
        for (int p0 = k0 - (k0 % 8); p0 < k0; ++p0)
        {
          res[j] += input_distributed[(p0 + i0 + j) % m0] * weights_distributed[p0];
        }

        // Store the result in the output array
        output_distributed[i0 + j] = res[j];
      }
    }

    // Distribute the results to other processes using MPI
    for (int dest_rid = 1; dest_rid < num_ranks; dest_rid++)
    {
      MPI_Send(output_distributed, m0, MPI_FLOAT, dest_rid, tag, MPI_COMM_WORLD);
    }

    // Receive results from other processes
    if (rid != root_rid)
    {
      MPI_Recv(output_distributed, m0, MPI_FLOAT, root_rid, tag, MPI_COMM_WORLD, &status);
    }
  }
  else
  {
    // Non-root processes receive computed results from the root process
  }
}

// Create the buffers on each node
void DISTRIBUTED_ALLOCATE_NAME(int m0, int k0,
                               float **input_distributed,
                               float **weights_distributed,
                               float **output_distributed)
{
  /*
    STUDENT_TODO: Modify as you please.
  */

  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if (rid == root_rid)
  {
    /* This block will only run on the node that matches root_rid .*/

    *input_distributed = (float *)malloc(sizeof(float) * m0);
    *output_distributed = (float *)malloc(sizeof(float) * m0);
    *weights_distributed = (float *)malloc(sizeof(float) * k0);
  }
  else
  {
    /* This will run on all other nodes whose rid is not root_rid. */
  }
}

void DISTRIBUTE_DATA_NAME(int m0, int k0,
                          float *input_sequential,
                          float *weights_sequential,
                          float *input_distributed,
                          float *weights_distributed)
{
  /*
    STUDENT_TODO: Modify as you please.
  */

  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if (rid == root_rid)
  {
    /* This block will only run on the node that matches root_rid .*/

    // Distribute the inputs
    #pragma omp parallel for
    for (int i0 = 0; i0 < m0; ++i0)
      input_distributed[i0] = input_sequential[i0];

    // Distribute the weights
    #pragma omp parallel for
    for (int p0 = 0; p0 < k0; ++p0)
      weights_distributed[p0] = weights_sequential[p0];
  }
  else
  {
    /* This will run on all other nodes whose rid is not root_rid. */
  }
}

void COLLECT_DATA_NAME(int m0, int k0,
                       float *output_distributed,
                       float *output_sequential)
{
  /*
    STUDENT_TODO: Modify as you please.
  */

  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if (rid == root_rid)
  {
    /* This block will only run on the node that matches root_rid .*/

    // Collect the output
    #pragma omp parallel for
    for (int i0 = 0; i0 < m0; ++i0)
      output_sequential[i0] = output_distributed[i0];
  }
  else
  {
    /* This will run on all other nodes whose rid is not root_rid. */
  }
}

void DISTRIBUTED_FREE_NAME(int m0, int k0,
                           float *input_distributed,
                           float *weights_distributed,
                           float *output_distributed)
{
  /*
    STUDENT_TODO: Modify as you please.
  */

  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if (rid == root_rid)
  {
    /* This block will only run on the node that matches root_rid .*/

    free(input_distributed);
    free(weights_distributed);
    free(output_distributed);
  }
  else
  {
    /* This will run on all other nodes whose rid is not root_rid. */
  }
}
